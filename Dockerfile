FROM node:14.18.0-alpine

RUN apk update
RUN apk upgrade
RUN apk add ca-certificates && update-ca-certificates 
# Change TimeZone
RUN apk add --update tzdata
ENV TZ=America/Sao_Paulo
# Clean APK cache
RUN rm -rf /var/cache/apk/*

WORKDIR /usr/app

COPY . .

RUN npm install

RUN npm run build

EXPOSE 3000

ENTRYPOINT ["sh"]

CMD [ "-c", "npm run start:prod" ]
