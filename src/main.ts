import { NestFactory } from '@nestjs/core';
import * as AWS from 'aws-sdk';
import { awsConfig } from './config/aws.config';
import { AppModule } from './modules/app/app.module';

// eslint-disable-next-line @typescript-eslint/no-var-requires
require('aws-sdk/lib/maintenance_mode_message').suppress = true;

async function bootstrap() {
  console.log('\x1Bc');

  console.log(`
  ████████╗███╗   ███╗██████╗      ██████╗ █████╗ ██╗      ██████╗██╗   ██╗██╗      █████╗ ████████╗ ██████╗ ██████╗ 
  ╚══██╔══╝████╗ ████║██╔══██╗    ██╔════╝██╔══██╗██║     ██╔════╝██║   ██║██║     ██╔══██╗╚══██╔══╝██╔═══██╗██╔══██╗
     ██║   ██╔████╔██║██████╔╝    ██║     ███████║██║     ██║     ██║   ██║██║     ███████║   ██║   ██║   ██║██████╔╝
     ██║   ██║╚██╔╝██║██╔══██╗    ██║     ██╔══██║██║     ██║     ██║   ██║██║     ██╔══██║   ██║   ██║   ██║██╔══██╗
     ██║   ██║ ╚═╝ ██║██████╔╝    ╚██████╗██║  ██║███████╗╚██████╗╚██████╔╝███████╗██║  ██║   ██║   ╚██████╔╝██║  ██║
     ╚═╝   ╚═╝     ╚═╝╚═════╝      ╚═════╝╚═╝  ╚═╝╚══════╝ ╚═════╝ ╚═════╝ ╚══════╝╚═╝  ╚═╝   ╚═╝    ╚═════╝ ╚═╝  ╚═╝
`);

  AWS.config.update({
    region: awsConfig.AWS_REGION,
    accessKeyId: awsConfig.ACCESS_KEY_ID,
    secretAccessKey: awsConfig.SECRET_ACCESS_KEY,
  });

  const app = await NestFactory.create(AppModule);
  await app.listen(3000);
}

bootstrap();
