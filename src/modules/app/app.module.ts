import { Module } from '@nestjs/common';
import { AppHandler } from './app.handler';
import { CalculatorService } from './calculator.service';
import { MessageModule } from '../message/message.module';

@Module({
  imports: [MessageModule],
  providers: [AppHandler, CalculatorService],
})
export class AppModule {}
