import { Injectable } from '@nestjs/common';
import { API_DATA } from '@vitacalc/common-types';

@Injectable()
export class CalculatorService {
  async calculate(data: API_DATA.DataToCalculate): Promise<number> {
    switch (data.gender) {
      case 'MALE':
        return await this.calculateMale(data);
      case 'FAMALE':
        return await this.calculateFamale(data);
      default:
        throw new Error('gender is not exists!');
    }
  }

  private async calculateMale(data: API_DATA.DataToCalculate): Promise<number> {
    return 0;
  }

  private async calculateFamale(
    data: API_DATA.DataToCalculate,
  ): Promise<number> {
    return 0;
  }
}
