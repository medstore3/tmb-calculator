import { SNS } from 'aws-sdk';
import { Injectable } from '@nestjs/common';
import { awsConfig } from 'src/config/aws.config';

@Injectable()
export class MessageProducer {
  private client: AWS.SNS;

  constructor() {
    this.client = new SNS({ endpoint: awsConfig.SNS_ENDPOINT });
  }

  async sendMessage<T extends object>(data: T, topicArn: string) {
    const params = {
      Message: JSON.stringify(data),
      TopicArn: topicArn,
    };

    const response = await this.client.publish(params).promise();

    console.log(
      `Message ${params.Message} sent to the topic ${params.TopicArn}`,
    );

    console.log('MessageID is ' + response.MessageId);

    return {
      MessageId: response.MessageId,
      SequenceNumber: response.SequenceNumber,
    };
  }
}
