import { Module } from '@nestjs/common';
import { MessageProducer } from './message.producer';
import { SqsModule } from '@ssut/nestjs-sqs';
import { awsConfig } from '../../config/aws.config';

@Module({
  imports: [
    SqsModule.register({
      consumers: [
        {
          name: awsConfig.QUEUE_NAME,
          queueUrl: awsConfig.QUEUE_URL,
          region: awsConfig.AWS_REGION,
        },
      ],
    }),
  ],
  providers: [MessageProducer],
  exports: [MessageProducer],
})
export class MessageModule {}
