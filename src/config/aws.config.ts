export const awsConfig = {
  AWS_REGION: process.env.AWS_REGION,
  ACCESS_KEY_ID: process.env.ACCESS_KEY_ID,
  SECRET_ACCESS_KEY: process.env.SECRET_ACCESS_KEY,
  QUEUE_NAME: process.env.QUEUE_NAME,
  QUEUE_URL: process.env.QUEUE_URL,
  TOPIC_ARN: process.env.TOPIC_ARN,
  SNS_ENDPOINT: process.env.SNS_ENDPOINT,
};
