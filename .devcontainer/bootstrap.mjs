import fs from 'fs'

const executeApp = async function () {
   const mainExists = fs.existsSync('/usr/app/dist/main.js');
   if (!mainExists) throw new Error('main.js is not exists');
   await import(`/usr/app/dist/main.js?v=${new Date()}`);
}

const bootstrap = async function () {
   console.log('transpiling...\n')
   executeApp()
      .catch(_ => {
         setTimeout(() => bootstrap(), 1000)
      });
}

bootstrap();
