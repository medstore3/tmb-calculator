module.exports = {
  apps: [
    {
      script: '/usr/app/.devcontainer/bootstrap.mjs',
      watch: ['/usr/app/src', '/usr/app/.devcontainer/pkg_volumes'],
      ignore_watch: ['dist'],
      watch_delay: 1000,
      node_args: ['--inspect'],
    },
  ],
};
